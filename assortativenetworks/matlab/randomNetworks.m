function randomNetworks(command, qtyOfVertices)
    clear global
    clearvars -except qtyOfVertices command        
    global n pN pA pD MAX_RANDOM_EDGES qtyOfSamplesByDegreeScale degreeScales fractionsInGiantComponent dataPath networksPath
    
    n = qtyOfVertices;
    pN = 0.5-0.25*sqrt(2);
    pA = 0.5;
    pD = 0.05;    
    
    MAX_RANDOM_EDGES = n/2;    
           
    degreeScales = [1 5 10 50 100];
    qtyOfSamplesByDegreeScale = 5;
    fractionsInGiantComponent = nan(length(degreeScales), qtyOfSamplesByDegreeScale);

    dataPath = ['data_n' num2str(qtyOfVertices)];
    networksPath = [dataPath '/networks'];

    if(strcontains(command, 'generate') || strcontains(command, 'g') || strcontains(command, 'all') || strcontains(command, 'a'))
        
        if(strcontains(command, 'p'))
            profile on;
        end
        tic
        generate();
        fprintf('>> Redes geradas em %f s\n', toc);
        if(strcontains(command, 'p'))
            profile viewer;
        end
    end
    
    if(strcontains(command, 'analyze') || strcontains(command, 'z') || strcontains(command, 'a') || strcontains(command, 'all'))        
        analyzeGiantComponents(dataPath);
    end
       
end

function generate()        
    global n dataPath networksPath pN pA pD qtyOfSamplesByDegreeScale degreeScales fractionsInGiantComponent
        
    if ~exist(dataPath, 'dir')
      mkdir(dataPath);
    end
    
    if ~exist(networksPath, 'dir')
      mkdir(networksPath);
    end
    
    for sample = 1 : qtyOfSamplesByDegreeScale
        
        for ds = 1 : length(degreeScales)
            
            degreeScale = degreeScales(ds);
            
            step = (sample-1) * length(degreeScales) + ds;
            overall = qtyOfSamplesByDegreeScale * length(degreeScales);
            
            fprintf('>> Iniciando gera��o %d de %d para ds=%d...\n', step, overall, degreeScale);
                        
            giantComponent = proceedCreation([pA, pN, pD], degreeScale, sample);            
            fractionsInGiantComponent(ds, sample, 1, 1) = giantComponent(1,1);
            fractionsInGiantComponent(ds, sample, 2, 1) = giantComponent(2,1);
            fractionsInGiantComponent(ds, sample, 3, 1) = giantComponent(3,1);
            
            fractionsInGiantComponent(ds, sample, 1, 2) = giantComponent(1,2);
            fractionsInGiantComponent(ds, sample, 2, 2) = giantComponent(2,2);
            fractionsInGiantComponent(ds, sample, 3, 2) = giantComponent(3,2);
        end
    end
        
    save([dataPath, '/giantComponents.mat'], 'fractionsInGiantComponent', 'degreeScales', 'n');

end

function giantComponent = proceedCreation(probabilities, degreeScale, sample)
    global pN networksPath n
    
    giantComponent = nan(3,2);

    for i = 1 : length(probabilities);
        probability = probabilities(i);
        newman = createNewmanNetwork(n, probability, degreeScale);

        if(probability > pN)
            assert(newman.r > 0);            
        else if(probability < pN)
            assert(newman.r < 0);
            else            
                assert(newman.r == 0);
            end
        end

        giantComponent(i,1) = newman.giantFraction;
        giantComponent(i,2) = newman.giantFractionOffset;

        filename = sprintf('%s/%s_kappa_%d_s_%d', networksPath, newman.type, degreeScale, sample);

        save([filename '.mat'], 'newman');
    %     toPajek([filename '.net'], newman.edges, newman.n);    
        clear newman;
    end
end