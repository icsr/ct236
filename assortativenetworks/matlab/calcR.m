function r = calcR(probability, degreeScale)
    p = probability;
    q = 1 - p;
    numerator = 8*p*q - 1;
    denominator = 2*exp(1/degreeScale)-1+2*(p-q)^2;
    r = numerator / denominator;
    
    if(abs(r) < 10^-10)
        r = 0;
    end
end