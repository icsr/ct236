function degreesDistribution = updateDegreesDistribution(degreesDistribution, degrees, v1, v2, n)
    
    degV1 = degrees(v1);
    degV2 = degrees(v2);
    if(~isKey(degreesDistribution, degV1))
        degreesDistribution(degV1) = 0;
    end

    if(~isKey(degreesDistribution, degV2))
        degreesDistribution(degV2) = 0;
    end

    degreesDistribution(degV1) = degreesDistribution(degV1) + 1/n;
    degreesDistribution(degV2) = degreesDistribution(degV2) + 1/n;

    degreesDistribution(degV1 - 1) = degreesDistribution(degV1 - 1) - 1/n;
    degreesDistribution(degV2 - 1) = degreesDistribution(degV2 - 1) - 1/n;
    
end