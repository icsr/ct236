function removeLastEdge()
    global Edges degrees
    
    v1v2 = Edges(end, :);
    Edges(end, :) = [];
        
    degrees(v1v2(1)) = degrees(v1v2(1)) - 1;
    degrees(v1v2(2)) = degrees(v1v2(2)) - 1;
end