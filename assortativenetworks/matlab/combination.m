function result = combination(n, p)
    result = 1;
    for i = 0 : p - 1
       result = result * (n-i); 
    end
    result = result / factorial(p);
end