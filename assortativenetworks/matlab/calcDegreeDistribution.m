function outcome = calcDegreeDistribution(n)
    global Edges degrees
       
    indexes = unique(Edges);
    degreesPerIndex = zeros(length(indexes), 2);
   
    degreesKeys = unique(degrees);
    
    m = size(degreesKeys, 1);    
    degreesDistribution = zeros(m, 2);
    
    for i = 1 : m       
        degreesDistribution(i, 1) = degreesKeys(i);
        degreesDistribution(i, 2) = length(find(degrees == degreesKeys(i)))/n;
    end
    
    outcome.Pk = degreesDistribution;
    outcome.Ik = degreesPerIndex;
    
    if(n == length(indexes))
        return;
    end
    
    degreesDistribution = [degreesDistribution; 0, (1 - length(indexes) / n)];
    outcome.Pk = degreesDistribution;
end