function edgeDistribution = calcEdgeDistribution(probability, degreeScale, J, K)
   N = 0.5 * (1-exp(-1/degreeScale));
   Cj = combination(J + K, J) * probability^J * (1-probability)^K;
   Ck = combination(J + K, K) * probability^K * (1-probability)^J;
   edgeDistribution = N * exp(-(J + K)/degreeScale) * (Cj + Ck);
end