function newman = createNewmanNetwork(n, probability, degreeScale) 
    clear global Edges ACTIVE DEACTIVE
    global Edges ACTIVE DEACTIVE;
    ACTIVE = 1;
    DEACTIVE = 0;
    initConfigs;
    
    randomNetwork = createRandomNetwork(n, degreeScale, probability);
    
    randomEdges = randomNetwork.edges;

    if(~isempty(Edges))
        Edges = [randomEdges, randomEdges(:,1) * 0 + 1];              
        doMonteCarloAlgorithm(probability, degreeScale);
        Edges(:, end) = [];
    end    
    
    newman.edges = Edges;
    newman.probability = probability;
    newman.r = calcR(probability, degreeScale);    
    newman.degreeScale = degreeScale;
    newman.n = n;
    newman.molloy = calcMolloy(newman.n);
    if(isempty(Edges))
        newman.componentsSizes = 0;
    else
        [~, newman.componentsSizes] = components(toSparseMatrix(newman.edges));
    end
    
    newman.giantFractionOffset = randomNetwork.giantFraction;
    newman.random = randomNetwork;
    
    newman.giantFraction = max(newman.componentsSizes) / newman.n;
        
    if(newman.r == 0)
        newman.type = 'neutral';
    else if(newman.r > 0)
            newman.type = 'assortative';
        else
            newman.type = 'disassortative';
        end
    end   
    
end

function doMonteCarloAlgorithm(probability, degreeScale)    
    global Edges degrees;

   lastQtyOfActiveEdges = 0;
    while(true)        
        qtyOfActiveEdges = size(getActiveEdges(), 1);              
        if(abs(lastQtyOfActiveEdges - qtyOfActiveEdges) <= 2)            
            break;
        end
        lastQtyOfActiveEdges = qtyOfActiveEdges;
        
        [v1w1, v1w1I, v2w2, v2w2I] = sortVertexPairs();
        
        j1k1 = [degrees(v1w1(1)), degrees(v1w1(2))] - 1;
        j2k2 = [degrees(v2w2(1)), degrees(v2w2(2))] - 1;

        ej1j2 = calcEdgeDistribution(probability, degreeScale, j1k1(1), j2k2(1));
        ek1k2 = calcEdgeDistribution(probability, degreeScale, j1k1(2), j2k2(2));
        ej1k1 = calcEdgeDistribution(probability, degreeScale, j1k1(1), j1k1(2));
        ej2k2 = calcEdgeDistribution(probability, degreeScale, j2k2(1), j2k2(2));
         
        dice = ej1j2 * ek1k2 / ej1k1 / ej2k2;
        dice = min(1, dice);        
        
        if(rand > dice)
            continue;
         end
        
        replaceEdges(v1w1I, v2w2I);
    end   
        
end

function [v1w1, v1w1I, v2w2, v2w2I] = sortVertexPairs() 
    global Edges;    
    
    activeEdges = getActiveEdges();
    m = size(activeEdges, 1);
    
    while(true)
        v1w1 = activeEdges(randi(m), :);        
        v1w1I = find(Edges(:,1) == v1w1(1) & Edges(:,2) == v1w1(2), 1);
        v2w2 = activeEdges(randi(m), :);
        v2w2I = find(Edges(:,1) == v2w2(1) & Edges(:,2) == v2w2(2), 1);
        if(v2w2I ~= v1w1I)
            break;
        end
    end        
    
    deactivateEdge(v1w1);
    deactivateEdge(v2w2);
end

function activeEdges = getActiveEdges()
    global Edges ACTIVE;
    activeEdges = Edges(Edges(:, 3) == ACTIVE, 1:2);
end

function deactivatedEdgeIndex = deactivateEdge(vertexPair)
    global Edges DEACTIVE;       
    deactivatedEdgeIndex = (Edges(:,1) == vertexPair(1) & Edges(:,2) == vertexPair(2));
    Edges(deactivatedEdgeIndex, 3) = DEACTIVE;
end