function random = createRandomNetwork(n, ds, pR)                
    global Edges degrees    
    
    createEdges(n, ds, pR);    
    random.edges = Edges;
    random.degrees = degrees;
    dist = calcDegreeDistribution(n);
    random.Pk = dist.Pk;
    random.Ik = dist.Ik;
    random.n = n;
    if(isempty(Edges))
        giantSize = 0;
    else
        [~, giantSize] = components(toSparseMatrix(random.edges)); 
    end
    random.giantFraction = max(giantSize) / n;    

end

function createEdges(n, ds, pR)    
    global Edges MAX_RANDOM_EDGES degrees distribution EdgesSize;
        
    Edges = [];
    EdgesSize = 0;
    
    degrees = zeros(n,1);
    distribution = nan(n, 1);
    distribution(0+1) = 1;
    
    MAX_MOLLOY = 0;
    
    if(ds == 1)
        return;
    end

    Edges = nan(MAX_RANDOM_EDGES, 2);
    v1 = 0;
    while(true)
        v1 = v1 + 1;
        if(v1 > n)
            break;
        end
        
        for v2 = v1 + 1 : n            
            if(EdgesSize >= MAX_RANDOM_EDGES)                
                return;
            end
                        
            if(degrees(v1) + 1> ds)                
                break;                
            end
            
            if(degrees(v2) + 1 > ds)                
                continue;
            end
            
            if(rand > pR)
                continue;
            end
            
        % >> checkMolloyAndAddEdgeIfOk - START            
            oldDegrees = degrees;
            oldDistribution = distribution;   

            degrees(v1) = degrees(v1) + 1;
            degrees(v2) = degrees(v2) + 1;

            if(isnan(distribution(degrees(v1) + 1)))
                distribution(degrees(v1) + 1) = 0;
            end

            if(isnan(distribution(degrees(v2) + 1)))
                distribution(degrees(v2) + 1) = 0;
            end

            distribution(degrees(v1) + 1) =  distribution(degrees(v1) + 1) + 1/n;
            distribution(degrees(v2) + 1) =  distribution(degrees(v2) + 1) + 1/n;

            distribution(oldDegrees(v1) + 1) =  distribution(oldDegrees(v1) + 1) - 1/n;
            distribution(oldDegrees(v2) + 1) =  distribution(oldDegrees(v2) + 1) - 1/n;

            distributionKeys = find(~isnan(distribution));
            distributionValues = distribution(distributionKeys);
            distributionKeys = distributionKeys - 1;

            molloy = distributionKeys .* (distributionKeys - 2) .* distributionValues;    
            molloy = sum(molloy);    

            molloyOk = molloy < MAX_MOLLOY;
            if(~molloyOk)
                degrees = oldDegrees;
                distribution = oldDistribution;
                continue;
            end
            
            EdgesSize = EdgesSize + 1;
            Edges(EdgesSize, :) = [v1, v2];            
        % >> checkMolloyAndAddEdgeIfOk - END;
            
        end           
    end   
end