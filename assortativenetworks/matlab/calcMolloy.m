function molloy = calcMolloy(n)
    global Edges

    if(isempty(Edges))
        molloy = -Inf;
        return;
    end
    
    dist = calcDegreeDistribution(n);
        
    Pk = dist.Pk;
    m = size(Pk, 1);
    
    molloy = 0;
    for id = 1 : m
        molloyI = Pk(id, 1);
        molloyLambdaI = Pk(id, 2);
        molloy = molloy + molloyI * (molloyI - 2)*molloyLambdaI;
    end
    
    if(abs(molloy) < 10^-10)
        molloy = 0;
    end

end