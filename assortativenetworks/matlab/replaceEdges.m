function replaceEdges(i1, i2)
    global Edges
    
    v1 = Edges(i1, 1);
    w1 = Edges(i1, 2);
    
    v2 = Edges(i2, 1);
    w2 = Edges(i2, 2);
    
    if(w1 == w2 || v1 == v2)
        return;
    end
    
    Edges(i1, 2) = v2;    
    Edges(i2, 1) = w1;
end