function toPajek(filepath, edges, n)

   [m, ~] = size(edges);
   
   content = sprintf('*Vertices %d\n*Edges', n);
   
   for i = 1 : m           
      content = sprintf('%s\n%d, %d\n%d, %d', content, edges(i,1), edges(i,2), edges(i,2), edges(i,1));
   end
   
   f = fopen(filepath, 'w+');
   fwrite(f, content);
   fclose(f);
end