function testSuite()
    global Edges degrees
    Edges = [
            1 2
            1 3
            1 4
            1 5
            2 3
            2 4
            2 5
            3 4
            3 5
            4 5
            ];
     degrees = [
                4;
                4;
                4;
                4;
                4
                ];
    
    testCalcDegreeDistribution();
    
    testCreateAssortativeNetwork();
    
    testCombination();
    
    testSparseMatrix();
    
    testCalcMolloy();
    
    testCalcR();
    
    tesCreateRegularPolygons();
    
%     testCalcEdgeDistribution();
    
end

function testCalcDegreeDistribution()    
    dist = calcDegreeDistribution(5);
    dist = dist.Pk;
    assert(dist(1) == 4);
    assert(dist(2) == 1);
    
    dist = calcDegreeDistribution(6);    
    dist = dist.Pk;
    assert(dist(1,1) == 4);
    assert(dist(1,2) - 5/6 < 10^-10);        
    assert(dist(2,1) == 0);
    assert(dist(2,2) - 1/6 < 10^-10)

end

function testCreateAssortativeNetwork()

%     edges.edges = [1 2; 1 6; 3 5; 3 4; 4 7; 4 8];
%     edges.n = 100;
%     createNewmanNetwork(10, edges, 0.5, 1);
    

end

function testCombination()

    assert(combination(5,2) == 10);
    assert(combination(5,1) == 5);
    assert(combination(5,0) == 1);

end

function testSparseMatrix()

    edges = [1 2; 1 6; 3 5; 3 4; 4 7; 4 8];
    sparseMatrix = toSparseMatrix(edges);    
    assert(sum(sum(full(sparseMatrix))) == 2*size(edges, 1));
    
    for i = 1 : size(edges, 1)
       assert(sparseMatrix(edges(i, 1), edges(i, 2)) == 1);
       assert(sparseMatrix(edges(i, 2), edges(i, 1)) == 1)
    end
end

function testCalcMolloy()
    global Edges degrees
    Edges = [1 2; 3 4; 3 5];
    degrees = [1; 1; 2; 1; 1];
    assert(calcMolloy(5) == -0.8);

end

function testCalcR()
    assert(abs(calcR(0.2, 1) - (8*0.2*0.8-1)/(2*exp(1)-1+2*0.6^2)) < 10^-10);    
end

function tesCreateRegularPolygons()

    oneR1 = createRegularPolygons(1, 1, 0);
    assert(isempty(oneR1.edges));
    assert(oneR1.lastIndex == 1);
    
    oneR2 = createRegularPolygons(1, 2, oneR1.lastIndex);
    assert(size(oneR2.edges, 1) == 1);
    assert(oneR2.lastIndex == 3);
    
    twoR2 = createRegularPolygons(2, 2, oneR1.lastIndex);
    assert(size(twoR2.edges, 1) == 2);
    assert(twoR2.lastIndex == 5);
    assert(twoR2.edges(1,1) == 2);
    assert(twoR2.edges(1,2) == 3);
    assert(twoR2.edges(2,1) == 4);
    assert(twoR2.edges(2,2) == 5);
    
    oneR3 = createRegularPolygons(1, 3, oneR2.lastIndex);
    assert(size(oneR3.edges, 1) == 3);
    assert(oneR3.lastIndex == 6);
    assert(oneR3.edges(1,1) == 4);
    assert(oneR3.edges(1,2) == 5);
    assert(oneR3.edges(2,1) == 4);
    assert(oneR3.edges(2,2) == 6);
    assert(oneR3.edges(3,1) == 5);
    assert(oneR3.edges(3,2) == 6);
        
    twoR4 = createRegularPolygons(2, 4, oneR3.lastIndex);
    assert(size(twoR4.edges, 1) == nchoosek(4,2) * 2);
    assert(twoR4.lastIndex == oneR3.lastIndex + 2 * 4);
    
    assert(twoR4.edges(1,1) == oneR3.lastIndex + 1);
    assert(twoR4.edges(1,2) == oneR3.lastIndex + 2);
    
    assert(twoR4.edges(2,1) == oneR3.lastIndex + 1);
    assert(twoR4.edges(2,2) == oneR3.lastIndex + 3);
    
    assert(twoR4.edges(3,1) == oneR3.lastIndex + 1);
    assert(twoR4.edges(3,2) == oneR3.lastIndex + 4);
    
    assert(twoR4.edges(4,1) == oneR3.lastIndex + 2);
    assert(twoR4.edges(4,2) == oneR3.lastIndex + 3);
    
    assert(twoR4.edges(5,1) == oneR3.lastIndex + 2);
    assert(twoR4.edges(5,2) == oneR3.lastIndex + 4);
    
    assert(twoR4.edges(6,1) == oneR3.lastIndex + 3);
    assert(twoR4.edges(6,2) == oneR3.lastIndex + 4);
end

function testCalcEdgeDistribution()
    global Edges degrees
    
    Edges = [
             1 2;
             3 4;
             4 5;
             4 6;
             4 7;
             ];
    degrees = [1; 1; 1; 4; 1; 1; 1];
      
    v1w1 = Edges(1, :);
    v2w2 = Edges(4, :);
    j1k1 = [degrees(v1w1(1)), degrees(v1w1(2))] - 1;
    j2k2 = [degrees(v2w2(2)), degrees(v2w2(2))] - 1;
    
    format long
    
    ej1j2 = calcEdgeDistribution(0.5, 1, j1k1(1), j2k2(1))
    ek1k2 = calcEdgeDistribution(0.5, 1, j1k1(2), j2k2(2))
    ej1k1 = calcEdgeDistribution(0.5, 1, j1k1(1), j1k1(2))
    ej2k2 = calcEdgeDistribution(0.5, 1, j2k2(1), j2k2(2))
    
    dice = ej1j2*ek1k2
    dice = ej1k1*ej2k2
    disp(dice);
    
    ej1j2 = calcEdgeDistribution(0.5, 100, j1k1(1), j2k2(1))
    ek1k2 = calcEdgeDistribution(0.5, 100, j1k1(2), j2k2(2))
    ej1k1 = calcEdgeDistribution(0.5, 100, j1k1(1), j1k1(2))
    ej2k2 = calcEdgeDistribution(0.5, 100, j2k2(1), j2k2(2))
    
    dice = ej1j2*ek1k2/ej1k1/ej2k2
    disp(dice);
        
    ej1j2 = calcEdgeDistribution(0.5, 100, j1k1(1), j2k2(1));
    
    assert(abs(ej1j2 - 4.896469409724506e-04) < 10^-10);
    assert(abs(ej1j2 - 0.002928348024469) < 10^-10);
    
end