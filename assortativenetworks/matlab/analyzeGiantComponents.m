function analysis = analyzeGiantComponents(dataPath)

    load([dataPath, '/giantComponents.mat']);
            
    dsL = size(fractionsInGiantComponent, 1);
    spL = size(fractionsInGiantComponent, 2);
    
    X = nan(dsL, 1);
    Y = nan(dsL, 3);
    V = nan(dsL, 3);
    Z = nan(dsL, 1);
    
    for ds = 1 : dsL                        
        PA = nan(spL, 1);
        PN = nan(spL, 1);
        PD = nan(spL, 1);
        ZEROS = nan(spL, 1);
        
        for sp = 1 : spL
            PA(sp) = fractionsInGiantComponent(ds, sp, 1, 1);
            PN(sp) = fractionsInGiantComponent(ds, sp, 2, 1);
            PD(sp) = fractionsInGiantComponent(ds, sp, 3, 1);
            ZEROS(sp) = fractionsInGiantComponent(ds, sp, 1, 2);
            
        end
        
        X(ds) = degreeScales(ds);
        Y(ds, 1) = zeroIfNegative(mean(PA));
        Y(ds, 2) = zeroIfNegative(mean(PN));
        Y(ds, 3) = zeroIfNegative(mean(PD));
        V(ds, 1) = var(PA);
        V(ds, 2) = var(PN);
        V(ds, 3) = var(PD);
        Z(ds, 1) = mean(ZEROS);
    end
    
    analysis.X = X;
    
    analysis.PA = Y(:,1);
    analysis.PN = Y(:,2);
    analysis.PD = Y(:,3);
    analysis.VPA = max(V(:,1));
    analysis.VPN = max(V(:,2));
    analysis.VPD = max(V(:,3));
        
    h = figure;
    semilogx(analysis.X, analysis.PA, 'ob-', analysis.X, analysis.PN, 'sk-' , analysis.X, analysis.PD, '^r-');       
    grid on
    legend(sprintf('Assortativa - var: %0.4f', analysis.VPA), sprintf('Neutra - var: %0.4f', analysis.VPN), sprintf('Desassortativa - var: %0.4f', analysis.VPD), 'Location','southeast');
    xlabel('Grau de Escala');
    ylabel('Fra��o de V�rtices na componente gigante');
    
    title(sprintf('Componentes Gigante para %d v�rtices', n));
    savefig(h, [dataPath, '/result.fig']);
    saveas(h, [dataPath, '/result.png']);
end

function adjusted = zeroIfNegative(value)

   adjusted = value;
   return;
   if(adjusted < 0)
       adjusted = 0;
   end

end