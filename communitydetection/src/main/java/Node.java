import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Node implements Serializable {

	private static final long serialVersionUID = 2050481816948930163L;

	private static int counterId = 0;
	public int id;
	public Node parent = null;
	public Node left = null;
	public Node right = null;
	public int[] indexes;
	
	public int[][] adjacencies;

	public static Node createRoot(int[][] adjacencies) throws Exception{
		
		counterId = 0;		
		return new Node(adjacencies);
		
	}
	
	public static Node createChild(Node parent, int[] indexes) throws Exception{
		if(parent == null){
			throw new Exception("Pai não pode ser nulo.");
		}
		
		return new Node(parent, indexes);
	}

	private Node(int[][] adjacencies) throws Exception {
		this.id = ++counterId;
		this.adjacencies = adjacencies;
	}
	
	private Node(Node parent, int[] indexes) throws Exception{
		this(parent.adjacencies);
		this.parent = parent;
		this.indexes = indexes;
		
		if (this.parent.left == null) {
			this.parent.left = this;
		} else if (this.parent.right == null) {
			this.parent.right = this;
		} else {
			throw new Exception("Os dois filhos já foram definidos");
		}
	}

	public boolean isRoot() {
		return this.parent == null;
	}

	public boolean isTerminal() {
		return this.left == null && this.right == null;
	}

	public int getHeight() {
		return getHeight(this);
	}

	private static int getHeight(Node node) {
		if (node == null || (node.left == null && node.right == null)) {
			return 0;
		}

		return 1 + Math.max(getHeight(node.left), getHeight(node.right));
	}

	public Node[] getTerminals() {

		List<Node> terminals = new ArrayList<Node>();
		findTerminals(this, terminals);

		Node[] terminalNodes = new Node[terminals.size()];

		return terminals.toArray(terminalNodes);

	}

	private static void findTerminals(Node root, List<Node> terminals) {

		if (root == null) {
			return;
		}

		if (root.left == null && root.right == null) {
			terminals.add(root);
			return;
		}

		findTerminals(root.left, terminals);
		findTerminals(root.right, terminals);

	}

	public String toDotFormat() {

		StringBuilder str = new StringBuilder("graph RedesSociais {\n\t");
		str.append("node[shape = \"circle\"];\n\t");
		str.append("rankdir=LR;\n\n");

		Node[] terminals = getTerminals();

		for (int i = 0; i < terminals.length; i++) {
			
			str.append(String.format("\tsubgraph cluster_%d{\n\t\t", i));
			str.append(String.format("label = \"Comunidade %d\"", i + 1));

			Node terminal = terminals[i];
			int[] terminalIndexes = terminal.indexes;

			String color = String.format(Locale.ENGLISH, "style=filled, fillcolor=\"%f %f %f\"", random(0.5,1), random(0.8, 1), random(0.8, 1));
			
			for(int terminalIndex : terminalIndexes){
				str.append(String.format("\n\t\t%d [%s]", terminalIndex, color));
			}
			
			for (int j = 0; j < terminalIndexes.length; j++) {
				
				
				int line = terminalIndexes[j];
				for (int k = j; k < terminalIndexes.length; k++) {
					int column = terminalIndexes[k];
					if (adjacencies[line - 1][column - 1] == 1) {
						String newTransition = String.format("\n\t\t%d -- %d;", line, column);
						str.append(newTransition);
					}
				}
			}
			
			str.append("\n\t}\n");

		}
		
		

		for (int i = 0; i < adjacencies.length; i++) {

			for (int j = i; j < adjacencies.length; j++) {
				if (adjacencies[i][j] == 1) {
					String newTransition = String.format("\t%d -- %d;\n", i + 1, j + 1);
					if (str.indexOf(newTransition) < 0) {
						str.append(newTransition);
					}
				}
			}
		}

		str.append("\n}");

		return str.toString();

	}

	public double random(double bottom, double top) {
		double number;
		do {
			number = Math.random();
		} while (number < bottom || number > top);
		return number;
	}

}