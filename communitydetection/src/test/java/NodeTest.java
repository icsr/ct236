import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Test;

@SuppressWarnings("unused")
public class NodeTest {

	private static Node R1;
	public static Node R4;

	@BeforeClass	
	public static void initRede4() throws Exception {

		R4 = Node.createRoot(new int[][] { 
				{ 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0 } });

		Node superA = Node.createChild(R4, null);
		Node superB = Node.createChild(R4, null);
		
		Node one = Node.createChild(superA, new int[] { 1, 2, 3, 4, 5 });

		Node two = Node.createChild(superA, new int[] { 6, 7, 8, 9, 10 });

		Node three = Node.createChild(superB, new int[] { 11, 12, 13, 14, 15 });

		Node four = Node.createChild(superB, new int[] { 16, 17, 18, 19, 20 });

	}

	@BeforeClass
	public static void initRede1() throws Exception {

		R1 = Node.createRoot(new int[][] { 
				{ 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0 },
				{ 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 
				{ 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0 }, 
				{ 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 
				{ 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1 },
				{ 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0 }, 
				{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1 },
				{ 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0 }, 
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1 }, 
				{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0 } });

		Node one = Node.createChild(R1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });

		Node two = Node.createChild(R1, new int[] { 9, 10, 11, 12, 13, 14 });

	}

	@Test
	public void testHeight() throws Exception {

		Node one = Node.createRoot(null);

		Node two = Node.createChild(one, null);
		Node three = Node.createChild(one, null);

		Node four = Node.createChild(two, null);
		Node five = Node.createChild(two, null);

		Node six = Node.createChild(four, null);
		Node seven = Node.createChild(five, null);

		Node eight = Node.createChild(seven, null);
		Node nine = Node.createChild(five, null);

		assertEquals(4, one.getHeight());

		Node ten = Node.createChild(nine, null);

		assertEquals(4, one.getHeight());

		Node eleven = Node.createChild(ten, null);

		assertEquals(5, one.getHeight());

	}

	@Test
	public void testIncludeThreeChildren() throws Exception {
		Node one = Node.createRoot(null);

		Node two = Node.createChild(one, null);
		Node tree = Node.createChild(one, null);

		Node four = Node.createChild(two, null);
		Node five = Node.createChild(two, null);

		Node six = Node.createChild(four, null);
		Node seven = Node.createChild(five, null);

		Node eight = Node.createChild(seven, null);

		Node nine = Node.createChild(five, null);
		try {
			Node ten = Node.createChild(five, null);
			fail("Deveria ter falhado ao tentar incluir o terceiro filho");
		} catch (Exception e) {
		}
	}

	@Test
	public void testTerminals() throws Exception {

		Node one = Node.createRoot(null);

		Node two = Node.createChild(one, null);
		Node tree = Node.createChild(one, null);

		Node four = Node.createChild(two, null);
		Node five = Node.createChild(two, null);

		Node six = Node.createChild(four, null);
		Node seven = Node.createChild(five, null);

		Node eight = Node.createChild(seven, null);
		Node nine = Node.createChild(five, null);

		Node ten = Node.createChild(nine, null);

		Node eleven = Node.createChild(ten, null);

		Node[] terminals = one.getTerminals();

		assertEquals(4, terminals.length);

	}

	@Test
	public void testDotFormat() throws Exception {

		 System.out.println(R4.toDotFormat());

//		System.out.println(R1.toDotFormat());

	}

}