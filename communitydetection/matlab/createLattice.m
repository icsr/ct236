function A = createLattice(n,r)

    A = zeros(n);
    
    for i = 1 : n       
        for j = 1 : r
            k = (i + j);
            if(k > n)
                k = mod(k, n);
            end            
            A(i, k) = 1;
            A(k, i) = 1;
        end
        
    end


end