function testSuite()
    close all;

    testCalcD();    
    testDetectCommunities();
    testCalcQ();    
    
end

function testCalcD()

    A = [
    0   1   1   0;
    1   0   1   0;
    1   1   0   1;
    0   0   1   0;
    ];

    D = calcD(A);
    assert(D(1,1)== 2);
    assert(D(2,2)== 2);
    assert(D(3,3)== 3);
    assert(D(4,4)== 1);

    assert(sum(D(1,:))== D(1,1));
    assert(sum(D(2,:))== D(2,2));
    assert(sum(D(3,:))== D(3,3));
    assert(sum(D(4,:))== D(4,4));

    clear A D
end

function testDetectCommunities()

    assert(2 == detectCommunities([0 0; 0 0], 'Rede 1'));
    assert(1 == detectCommunities([0 1; 1 0], 'Rede 2'));
    assert(3 == detectCommunities([0 0 0; 0 0 0; 0 0 0], 'Rede 3'));
    assert(1 == detectCommunities([0 1 1; 1 0 1; 1 1 0], 'Rede 4'));
    
    assert(1 == detectCommunities([0 1; 1 0], 'Rede 5'));
    assert(2 == detectCommunities([0 1 0; 1 0 0; 0 0 0], 'Rede 6'));
    assert(1 == detectCommunities([0 1 1; 1 0 1; 1 1 0], 'Rede 7'));
    assert(2 == detectCommunities([0 1 0 0; 1 0 0 0; 0 0 0 1; 0 0 1 0], 'Rede 8'));
    assert(2 == detectCommunities([0 0 0 0; 0 0 0 1; 0 0 0 1; 0 1 1 0], 'Rede 9'));
    assert(3 == detectCommunities([0 0 0 0 0; 0 0 0 1 0; 0 0 0 1 0; 0 1 1 0 0; 0 0 0 0 0], 'Rede 10'));
    assert(3 == detectCommunities([0 1 1 0 0 0 1 0 0 0; 1 0 1 0 0 0 1 0 0 0; 1 1 0 0 0 1 1 0 0 0; 0 0 0 0 1 1 0 0 0 0; 0 0 0 1 0 1 0 0 0 0; 0 0 1 1 1 0 0 0 0 0; 1 1 1 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0 1 1; 0 0 0 0 0 0 0 1 0 1; 0 0 0 0 0 0 0 1 1 0], 'Rede 11'));
    
    assert(4 == detectCommunities([0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;1 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;1 1 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;1 1 1 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 1 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 1 0 1 1 1 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 1 1 1 0 1 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 1 1 1 1 0 1 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 1 0 1 1 1 1 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 1 0 1 1 1 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 1 1 1 0 1 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 1 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 1 1;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 1;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 1;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0], 'Rede R2A'));

end

function testCalcQ()

    [~, root] = detectCommunities([0 1; 1 0], 'Rede 1');
    assert(0 == calcQ(root));
    
    [~, root] = detectCommunities([0 0; 0 0], 'Rede 1');
    assert(isnan(calcQ(root)));
    
     [~, root] = detectCommunities([0 1 1; 1 0 1; 0 1 1], 'Rede 1');
    assert(0 == calcQ(root));
    
    [~, root] = detectCommunities([0 1 1; 1 0 0; 0 0 1], 'Rede 1');    
    assert(0.125 == calcQ(root));

end