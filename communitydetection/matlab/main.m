close all
clear
clear java
clc

javaaddpath('./java');
load('data.mat');

[R1VC, R1VL] = calcLaplacian(R1A);
[R2VC, R2VL] = calcLaplacian(R2A);
[R3VC, R3VL] = calcLaplacian(R3A);
[R4VC, R4VL] = calcLaplacian(R4A);

[R1K, root1] = detectCommunities(R1A, 'Rede 1');
[R2K, root2] = detectCommunities(R2A, 'Rede 2');
[R3K, root3] = detectCommunities(R3A, 'Rede 3');
[R4K, root4] = detectCommunities(R4A, 'Rede 4');

R1Q = calcQ(root1);
R2Q = calcQ(root2);
R3Q = calcQ(root3);
R4Q = calcQ(root4);

plotOutcome(4, 'Rede 1', 1, R1VC, R1VL, R1K, R1Q, root1);
plotOutcome(4, 'Rede 2', 2, R2VC, R2VL, R2K, R2Q, root2);
plotOutcome(4, 'Rede 3', 3, R3VC, R3VL, R3K, R3Q, root3);
plotOutcome(4, 'Rede 4', 4, R4VC, R4VL, R4K, R4Q, root4);



save('outcome/outcome.mat');

toFile('outcome/rede1.dot', root1);
toFile('outcome/rede2.dot', root2);
toFile('outcome/rede3.dot', root3);
toFile('outcome/rede4.dot', root4);

clear
