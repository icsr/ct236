function [VC, VL] = calcLaplacian(A) 
    L = calcD(A) - A;
    [~, VL, VC] = eig(L);
    VL = diag(VL);
    
    VL = correctZeros(VL);
    VC = correctZeros(VC);
    
end