function plotOutcome(nc, plotTitle, p, VC, VL, K, Q, rootNode)

    m = 3;

    terminals = rootNode.getTerminals();
    
    q1 = p + nc;
    q2 = q1 + nc;

    for i = 1 : terminals.length
        
        communityColor = [rootNode.random(0.2, 0.8), rootNode.random(0, 0.8), rootNode.random(0, 0.8)];
        indexes = terminals(i).indexes; 
                              
        subplot(m,nc,p);    
        hold on
        plot(indexes, VL(indexes), 'o',  'MarkerEdgeColor', communityColor, 'MarkerFaceColor', communityColor);        
        title(plotTitle);
        grid on        
        xlabel('V�rtice da rede');
        ylabel('Autovalor');
        
        subplot(m,nc,q1);
        hold on               
        plot(indexes, VC(indexes, getIndex(VL)), 'o',  'MarkerEdgeColor', communityColor, 'MarkerFaceColor', communityColor);        
        grid on
        title([plotTitle, ' - ' num2str(K) ' comunidades detectadas' ]);
        xlabel('V�rtice da rede');
        ylabel('Autovetor');
        
        subplot(m,nc,q2);    
        hold on
        plot(VC(indexes, getIndex(VL)), zeros(length(indexes), 1), 'o',  'MarkerEdgeColor', communityColor, 'MarkerFaceColor', communityColor);        
        title([plotTitle, ' - modularidade: ' num2str(Q) ]);
        grid on        
        xlabel('Autovalor');
        ylabel('Espectro');
            
    end
    
    hold off
    
    fprintf(getStatus(plotTitle, rootNode.adjacencies, terminals, Q));

end