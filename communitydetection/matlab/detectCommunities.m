function [qtyOfCommunities, root] = detectCommunities(A, title)
    
    [m, ~] = size(A);
    indexes = 1 : m;
   
    if(nargin == 1)
        title = 'Rede';
    end
    
    format long

    root = Node.createRoot(A);    
    performDetection(A, root, indexes);
    
    terminals = root.getTerminals();
    
    qtyOfCommunities = terminals.length;
end

function node = performDetection(A, parent, indexes)
   
    [VC, VL] = calcLaplacian(A);
        
    node = Node.createChild(parent, indexes);
    
    partition1Idxs = find(VC(:, getIndex(VL)) > 0);
    partition2Idxs = find(VC(:, getIndex(VL)) <= 0);
    
    if(isempty(partition1Idxs) || isempty(partition2Idxs))
        partition1Idxs = find(VC(:, getIndex(VL)) >= 0);
        partition2Idxs = find(VC(:, getIndex(VL)) < 0);
    end
    
     if(isempty(partition1Idxs) || isempty(partition2Idxs))
         return;
     end
    
    A1 = A(partition1Idxs, partition1Idxs);
    indexesA1 = indexes(partition1Idxs);
    
    A2 = A(partition2Idxs, partition2Idxs);
    indexesA2 = indexes(partition2Idxs);
    
        
    performDetection(A1, node, indexesA1);
    performDetection(A2, node, indexesA2);
   
end