function status = getStatus(title, A, terminals, Q)

    qtyOfCommunities = terminals.length;

    status = sprintf('Status da %s: %d comunidades detectadas em %s', title, qtyOfCommunities, mat2str(A));        
    for i = 1 : qtyOfCommunities        
        status = sprintf('%s\n\tComunidade %d: %s', status, i, mat2str(terminals(i).indexes'));        
    end
    
    status = sprintf('%s\n\tModularidade: %f', status, Q);
    
    status = sprintf('%s\n', status);
    
end