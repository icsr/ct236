function toFile(filepath, node)

    fileID = fopen(filepath, 'w+');
    fwrite(fileID, node.toDotFormat().getBytes());
    fclose(fileID);

end