function M = correctZeros(M)

    [m, n] = find((abs(M) <= 10^-10 & M ~= 0));
    
    for i = 1 : length(m)
        M(m(i), n(i)) = 0;
    end


end