function D = calcD(A)

    [m, n] = size(A);
    if(m ~= n)
        D = nan(m);
        error('matriz de adjac�ncias deve ser quadrada!');        
    end
    
    D = zeros(m);
    
    for i = 1 : m
       for j = 1 : m
          D(i,i) = D(i,i) + A(i,j);
       end
    end

end