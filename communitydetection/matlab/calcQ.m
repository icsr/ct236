function Q = calcQ(rootNode)

    adjacencies = rootNode.adjacencies;
    terminals = rootNode.getTerminals();
    Q = 0;
    
    E = sum(sum(adjacencies)) / 2;
    
    for i = 1 : terminals.length
        indexes = terminals(i).indexes;
        Ei = sum(sum(adjacencies(indexes, indexes)) / 2);
        sg2 = (sum(sum(adjacencies(indexes, :))))^2;        
        
        Q = Q + Ei / E - (4 * E^2)^(-1) * sg2;        
    end

end