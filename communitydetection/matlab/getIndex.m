function k = getIndex(VL)

    k = min(find((VL < 1 & VL > 0)));
    if(~isempty(k))
        return;
    end
    
    k = max(find(VL == 0));    
end