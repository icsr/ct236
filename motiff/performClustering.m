function outcome = performClustering(maxDistance, atts, weights)
    global data
    
    m = size(data, 1);
    outcome.edges = [];
    
    w = waitbar(0, sprintf('Performin clustering for %d instances\nand max distance %f', m, maxDistance));
    mapDistances = cell(m, 1);
    
    imax = m - 1;
    jmax = m;
    kmax = length(atts);
    for i = 1 : imax
        waitbar(i / imax);
        clear distances
        distances = nan(m - i, 2);
        for j = i + 1 : jmax            
            index = j - i;
            distances(index, 2) = 0;
            distances(index, 1) = j;
            for k = 1 : kmax
                if(isnan(data(i, atts(k))) || isnan(data(j, atts(k))))
                    continue;
                end
                attributeValue = abs(data(i, atts(k)) - data(j, atts(k)));
                maxAttributeValue = max(data(~isnan(data(:, atts(k))), atts(k)));                 
                
                if(maxAttributeValue == 0)
                    maxAttributeValue = 1;
                end
                distances(index, 2) = distances(index, 2) + weights(k) * attributeValue / maxAttributeValue;                
            end
            if(distances(index, 2) <= maxDistance)
                outcome.edges = [outcome.edges; i, j];
            end
        end
        
        mapDistances(i) = {distances};
        
        
    end   
    close(w);
    outcome.mapDistances = mapDistances;    
    outcome.n = m;
    outcome.maxDistance = maxDistance;
end