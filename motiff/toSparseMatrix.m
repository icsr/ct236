function sparseMatrix = toSparseMatrix(edges)
    edges = [edges; edges(:, 2), edges(:, 1)];

    sparseMatrix = sparse(edges(:,1), edges(:,2), edges(:,1) * 0 + 1);
end