function toPajek(filepath, edges, n, attsAndWeights)
   global data
   
   clear content;
   
   labels = struct2table(getLabels());
   labels = labels.Properties.VariableNames(attsAndWeights(:,1));   
   
   for i = 1 : size(attsAndWeights(:,1))       
        labels(i) = {sprintf('%s (i=%d, w=%f)', cell2mat(labels(i)), attsAndWeights(i,1), attsAndWeights(i,2))};
   end
   
   content = sprintf('*Vertices %d', n);
   for i = 1 : n
       label = '';
       for j = 1 : size(attsAndWeights(:,1))
            label = sprintf('%s%s: %f; ', label, cell2mat(labels(j)), data(i, attsAndWeights(j, 1))); 
       end
       content = sprintf('%s\r\n%d \"%s"', content, i, label);
   end   
   
   content = sprintf('%s\r\n*Edges', content);
   m = size(edges, 1);
   w = waitbar(0, sprintf('Salvando %d edges...', m));
   for i = 1 : m   
      content = sprintf('%s\r\n%d, %d\r\n%d, %d', content, edges(i,1), edges(i,2), edges(i,2), edges(i,1));
      waitbar(i/m, w);
   end
   content = sprintf('%s\r\n', content);
   
   f = fopen(filepath, 'w+');
   fwrite(f, content);
   fclose(f);
   
   close(w);
end