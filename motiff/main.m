function main()
    clc
    clear
    clear global
    global data 
    initConfigs();
    
    if(~exist('resultados', 'dir'))
        mkdir('resultados');
    end

    labels = getLabels();    

    attsAndWeights1 = [
            labels.NU_DUR_ATIV_COMP_MESMA_REDE, 1;
            labels.NU_DUR_ATIV_COMP_OUTRAS_REDES, 1;
            labels.FK_COD_MOD_ENSINO, 1;            
            labels.FK_COD_ETAPA_ENSINO, 1;            
            labels.PK_COD_TURMA, 1;       
            labels.FK_COD_TIPO_TURMA, 1;
            labels.ID_DEPENDENCIA_ADM_ESC, 1;
            ];
    
    data = csvread('data.csv');
    
    maxDistances = [10^-5, 10^-4, 10^-3, 10^-2, 10^-1];
       
    performAndSave(1, maxDistances, attsAndWeights1);
    
    
end

function allComponents = performAndSave(dataId, maxDistances, attsAndWeights)
   
    path = sprintf('resultados/R%d', dataId);
    if(~exist(path, 'dir'))
        mkdir(path);
    end

    attsAndWeights(:,2) = attsAndWeights(:,2) ./ sum(attsAndWeights(:, 2)); 
    
    allComponents = cell(length(maxDistances),1);
    for i = 1 : length(maxDistances)
        clear outcome;
        fprintf('\nCriando rede %d/%d...', i, length(maxDistances));
        outcome = performClustering(maxDistances(i), attsAndWeights(:,1), attsAndWeights(:,2));
        [outcome.components, outcome.componentsSize] = components(toSparseMatrix(outcome.edges));
        
        labels = getLabels();
        save(sprintf('%s/redes_%f.mat', path, outcome.maxDistance), 'outcome', 'attsAndWeights', 'labels');
        toPajek(sprintf('%s/redes_%f.net', path, outcome.maxDistance), outcome.edges, outcome.n, attsAndWeights);
        
        fprintf('\nRede criada como componente gigante tendo %d de %d v�rtices', max(outcome.componentsSize), outcome.n);
    end
end